/**
 * Filename: response-object.ts
 * Author: rnunez@akurey.com
 * Date: 12/20/2018
 * Description: To encapsulate the response object to be send in response to the frontend
 */
import { Request } from 'express';
export declare class ResponseObject {
    data: any;
    elapseTime: number;
    clientTimeStamp: Date;
    serverTimeStamp: Date;
    constructor(pData: any, pReq: Request);
}
