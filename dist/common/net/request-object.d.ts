/**
 * Filename: request-object.ts
 * Author: rnunez@akurey.com
 * Date: 12/20/2018
 * Description: To encapsulate the request object sent in all rest messages
 */
import { Credential } from '../security/credential';
export declare class RequestObject {
    data: any;
    computer: string;
    ip: string;
    clientTimeStamp: Date;
    serverTimeStamp: Date;
    private credential;
    private secure;
    constructor(pCredential: Credential, pComputer: string, pIp: string, pData: any);
    setSecure(pValue: string): void;
}
