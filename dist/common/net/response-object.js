"use strict";
/**
 * Filename: response-object.ts
 * Author: rnunez@akurey.com
 * Date: 12/20/2018
 * Description: To encapsulate the response object to be send in response to the frontend
 */
Object.defineProperty(exports, "__esModule", { value: true });
var ResponseObject = /** @class */ (function () {
    function ResponseObject(pData, pReq) {
        this.data = pData;
        this.clientTimeStamp = pReq.body.clientTimeStamp;
        this.serverTimeStamp = new Date();
        this.elapseTime = (new Date()).getTime() - pReq.body.clientTimeStamp.getTime();
    }
    return ResponseObject;
}());
exports.ResponseObject = ResponseObject;
//# sourceMappingURL=response-object.js.map