"use strict";
/**
 * Filename: request-object.ts
 * Author: rnunez@akurey.com
 * Date: 12/20/2018
 * Description: To encapsulate the request object sent in all rest messages
 */
Object.defineProperty(exports, "__esModule", { value: true });
var core_crypto_1 = require("../security/core-crypto");
var RequestObject = /** @class */ (function () {
    function RequestObject(pCredential, pComputer, pIp, pData) {
        this.data = pData;
        this.computer = pComputer ? pComputer : '';
        this.ip = pIp ? pIp : '';
        this.clientTimeStamp = new Date();
        this.serverTimeStamp = new Date();
        this.credential = pCredential;
        this.secure = {};
    }
    RequestObject.prototype.setSecure = function (pValue) {
        this.secure = core_crypto_1.CoreCrypto.encrypt(pValue, this.credential.publicKey);
    };
    return RequestObject;
}());
exports.RequestObject = RequestObject;
//# sourceMappingURL=request-object.js.map