/**
 * Filename: logger-options.ts
 * Author: rnunez@akurey.com
 * Date: 03/08/2019
 * Description: Options to setup wiston logger
 */
import winston from 'winston';
export declare const loggerColors: {
    error: string;
    warn: string;
    info: string;
    verbose: string;
    debug: string;
    event: string;
};
export declare const loggerOptions: {
    colorize: boolean;
    meta: boolean;
    levels: {
        error: number;
        warn: number;
        info: number;
        verbose: number;
        debug: number;
        event: number;
    };
    transports: (winston.transports.ConsoleTransportInstance | winston.transports.FileTransportInstance)[];
};
