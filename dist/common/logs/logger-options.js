"use strict";
/**
 * Filename: logger-options.ts
 * Author: rnunez@akurey.com
 * Date: 03/08/2019
 * Description: Options to setup wiston logger
 */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var winston_1 = __importDefault(require("winston"));
// Configure dotenv for custom process.env variables
var env = process.env.NODE_ENV || 'local';
var isLocal = env === 'local';
// const filePath = (isLocal) ? './logs/' : './logs/';
var filePath = './logs/';
exports.loggerColors = {
    error: 'red',
    warn: 'white',
    info: 'white',
    verbose: 'green',
    debug: 'blue',
    event: 'yellow',
};
exports.loggerOptions = {
    colorize: true,
    meta: true,
    levels: {
        error: 0,
        warn: 1,
        info: 2,
        verbose: 3,
        debug: 4,
        event: 5,
    },
    transports: [
        new winston_1.default.transports.Console({
            format: winston_1.default.format.combine(winston_1.default.format.colorize(), winston_1.default.format.json(), winston_1.default.format.prettyPrint(), winston_1.default.format.printf(function (info) { return "[" + info.level + "]: " + info.message; })),
            level: 'verbose',
        }),
        new winston_1.default.transports.File({
            filename: filePath + "error.log",
            format: winston_1.default.format.combine(winston_1.default.format.json(), winston_1.default.format.splat()),
            level: 'error',
        }),
        new winston_1.default.transports.File({
            filename: filePath + "info.log",
            format: winston_1.default.format.combine(winston_1.default.format.json(), winston_1.default.format.splat()),
            level: 'info',
        }),
        new winston_1.default.transports.File({
            filename: filePath + "verbose.log",
            format: winston_1.default.format.combine(winston_1.default.format.json(), winston_1.default.format.splat()),
            level: 'verbose',
        }),
        new winston_1.default.transports.File({
            filename: filePath + "events.log",
            format: winston_1.default.format.combine(winston_1.default.format.json(), winston_1.default.format.splat()),
            level: 'event',
        }),
    ],
};
//# sourceMappingURL=logger-options.js.map