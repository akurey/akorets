"use strict";
/**
 * Filename: log.util.ts
 * Author: lcruz@akurey.com
 * Date: 12/12/2018
 * Description: Log Utility which exports log config and winston logger
 * Modify by rnunez in order to standarize the log object
 */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var crypto_1 = __importDefault(require("crypto"));
var winston_1 = __importDefault(require("winston"));
var logger_options_1 = require("./logger-options");
var log_1 = require("./log");
// Configure dotenv for custom process.env variables
var env = process.env.NODE_ENV || 'local';
var Logger = /** @class */ (function () {
    /**
     * Initialize the queue and wiston logger
     */
    function Logger() {
        this.logger = winston_1.default.createLogger(logger_options_1.loggerOptions);
        winston_1.default.addColors(logger_options_1.loggerColors);
    }
    Logger.getInstance = function () {
        if (!Logger.instance) {
            Logger.instance = new Logger();
        }
        return Logger.instance;
    };
    /**
     * Log a verbose msg
     * @param pMsg
     */
    Logger.prototype.verbose = function (pMsg) {
        this.logger.log('verbose', pMsg);
    };
    /**
     * Log error messages
     * @param pMsg
     */
    Logger.prototype.error = function (pMsg) {
        this.logger.log('error', pMsg);
    };
    /**
    * Log info messages
    * @param pMsg
    */
    Logger.prototype.info = function (pMsg) {
        this.logger.log('info', pMsg);
    };
    /**
    * Log event messages
    * @param pMsg
    */
    Logger.prototype.event = function (pMsg) {
        this.logger.log('event', pMsg);
    };
    /**
    * Log info messages
    * @param pMsg
    */
    Logger.prototype.warn = function (pMsg) {
        this.logger.warn('warn', pMsg);
    };
    Logger.prototype.log = function (pType, pDescription, pCredential, pLogTime, pDetails, pResponse, pTarget1, pTarget2, pRefValue1, pRefValue2) {
        var log = new log_1.Log(pType, pDescription);
        log.computer = pCredential && pCredential.computer ? pCredential.computer : '';
        log.user = pCredential && pCredential.user ? pCredential.user : '';
        log.ip = pCredential && pCredential.ip ? pCredential.ip : '';
        log.logtime = new Date(pLogTime) || log.logtime;
        log.details = pDetails || '';
        log.response = pResponse || '';
        log.target1 = pTarget1 || '';
        log.target2 = pTarget2 || '';
        log.refvalue1 = pRefValue1 || '';
        log.refvalue2 = pRefValue2 || '';
        log.responsetime = log.posttime.getTime() - log.logtime.getTime();
        var hmac = crypto_1.default.createHmac('sha256', '2782l893j83kdkld93jdp2kds9mkkf9820dkdj810wsk');
        var checkedValue = "1111" + log.type + log.refvalue1 + "893jkf9820dk" + log.refvalue2 + log.target1 + "2782ldj810wsk" + log.target2 + "---" + log.logtime + "--rnunez_inthefloor-" + log.posttime + log.user + "83kdkld93jdp2kds9mk-drSpock" + log.computer + log.ip;
        hmac.update(checkedValue);
        log.checksum = hmac.digest('hex');
        this.logger.log('event', log);
        this.logger.info('event', log);
        this.logger.event('event', log);
        // this.logger.log('event', JSON.stringify(log));
    };
    return Logger;
}());
exports.Logger = Logger;
//# sourceMappingURL=logger.js.map