"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Filename: log.util.ts
 * Author: rnunez
 * Date: 03/05/2019
 * Description: Generic log object to be stored
 */
var Log = /** @class */ (function () {
    function Log(pType, pDescription) {
        this.type = pType;
        this.description = pDescription;
        this.computer = '';
        this.ip = '';
        this.user = '';
        this.posttime = new Date();
        this.logtime = new Date();
        this.responsetime = 0;
        this.details = '';
        this.response = '';
        this.target1 = '';
        this.target2 = '';
        this.refvalue1 = '';
        this.refvalue2 = '';
        this.checksum = '';
    }
    return Log;
}());
exports.Log = Log;
//# sourceMappingURL=log.js.map