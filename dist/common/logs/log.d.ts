/**
 * Filename: log.util.ts
 * Author: rnunez
 * Date: 03/05/2019
 * Description: Generic log object to be stored
 */
export declare class Log {
    type: string;
    description: string;
    computer: string;
    ip: string;
    user: string;
    posttime: Date;
    responsetime: number;
    logtime: Date;
    details: string;
    response: string;
    target1: string;
    target2: string;
    refvalue1: string;
    refvalue2: string;
    checksum: string;
    constructor(pType: string, pDescription: string);
}
