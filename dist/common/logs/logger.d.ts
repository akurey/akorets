/**
 * Filename: log.util.ts
 * Author: lcruz@akurey.com
 * Date: 12/12/2018
 * Description: Log Utility which exports log config and winston logger
 * Modify by rnunez in order to standarize the log object
 */
import winston from 'winston';
import { Credential } from '../security/credential';
export declare class Logger {
    static getInstance(): Logger;
    private static instance;
    logger: winston.Logger;
    /**
     * Initialize the queue and wiston logger
     */
    private constructor();
    /**
     * Log a verbose msg
     * @param pMsg
     */
    verbose(pMsg: string): void;
    /**
     * Log error messages
     * @param pMsg
     */
    error(pMsg: string): void;
    /**
    * Log info messages
    * @param pMsg
    */
    info(pMsg: string): void;
    /**
    * Log info messages
    * @param pMsg
    */
    warn(pMsg: string): void;
    /**
     * General log action with multiple variations
     * @param pType
     * @param pDescription
     * @param pCredential
     * @param pLogTime
     */
    log(pType: string, pDescription: string, pCredential: Credential, pLogTime: string): void;
    log(pType: string, pDescription: string, pCredential: Credential, pLogTime: string, pDetails: string): void;
    log(pType: string, pDescription: string, pCredential: Credential, pLogTime: string, pDetails: string, pResponse?: string): void;
    log(pType: string, pDescription: string, pCredential: Credential, pLogTime: string, pDetails?: string, pResponse?: string, pTarget1?: string, pTarget2?: string, pRefValue1?: string, pRefValue2?: string): void;
}
