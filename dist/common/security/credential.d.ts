/**
 * Filename: credential.ts
 * Author: rnunez@akurey.com
 * Date: 12/20/2018
 * Description: Object to store user information regarding its identity, tokens and cryptograpy keys
 */
import { CredentialType } from './credential-type';
export declare class Credential {
    user: string;
    publicKey: string;
    computer: string;
    ip: string;
    groupList: string[];
    private tokens;
    constructor(pUser: string, pPublicKey: string);
    addToken(pCredentialType: CredentialType, pValue: string): void;
    getToken(pCredentialType: CredentialType): string;
    setDeviceName(pName: string): void;
    setDeviceIp(pIp: string): void;
}
