/**
 * Filename: credential-type.ts
 * Author: rnunez@akurey.com
 * Date: 12/20/2018
 * Description: Enums to manage encryption and credentials
 */
export declare enum CredentialType {
    CUSTOM = 1,
    AWS = 2,
    AWS_COGNITO_AUTHO_TOKEN = 3,
    AWS_COGNITO_ID_TOKEN = 4,
    POWER_BI = 5
}
export declare enum KeyType {
    PRIVATE = 1,
    PUBLIC = 2
}
