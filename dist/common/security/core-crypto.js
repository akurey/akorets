"use strict";
/**
 * Filename: core-crypto.ts
 * Author: rnunez@akurey.com
 * Date: 12/20/2018
 * Description: Encrypt and decrypt functions based on RSA of auto generated keys
 */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var node_rsa_1 = __importDefault(require("node-rsa"));
var ENCRYPT_ENCODING = 'base64';
var DECRYPT_ENCODING = 'utf8';
var WAIT_TIME_KEY_CREATION = 0;
var CoreCrypto = /** @class */ (function () {
    /**
     * Creates a new instance capable of encrypt/decrypt
     * @param pGenerateNewKeys , if true, new keys are generated, by the contrary the keys must be provide properly
     */
    function CoreCrypto(pGenerateNewKeys) {
        var _this = this;
        this.privateKey = '';
        this.publicKey = '';
        if (pGenerateNewKeys) {
            this.createKeyPair()
                .then(function (keys) {
                _this.publicKey = keys.publicKey;
                _this.privateKey = keys.privateKey;
            })
                .catch(function () {
                _this.publicKey = '';
                _this.privateKey = '';
            });
        }
    }
    CoreCrypto.prototype.getPublicKey = function () {
        return this.publicKey;
    };
    CoreCrypto.prototype.getPrivateKey = function () {
        return this.privateKey;
    };
    /**
     * Asymetric encryption based on given key
     * @param pData text to be encrypted
     * @param pKey RSA private/public key to be use for encryption
     */
    CoreCrypto.encrypt = function (pData, pKey) {
        var key = new node_rsa_1.default(pKey);
        return key.encrypt(pData, ENCRYPT_ENCODING);
    };
    /**
     * Asymetric decryption based on given key
     * @param pData text to be decrypted
     * @param pKey RSA private key to be use for decryption
     */
    CoreCrypto.decrypt = function (pData, pPrivateKey) {
        var key = new node_rsa_1.default(pPrivateKey);
        return key.decrypt(pData, DECRYPT_ENCODING);
    };
    /**
     * Generate one asymetric key pair
     */
    CoreCrypto.prototype.createKeyPair = function () {
        return new Promise(function (resolve, reject) {
            setTimeout(function () {
                try {
                    var key = new node_rsa_1.default({ b: 2048 });
                    var privateKey = key.exportKey(), publicKey = key.exportKey('public');
                    resolve({ privateKey: privateKey, publicKey: publicKey });
                }
                catch (_a) {
                    reject({ privateKey: '', publicKey: '' });
                }
            }, WAIT_TIME_KEY_CREATION);
        });
    };
    ;
    return CoreCrypto;
}());
exports.CoreCrypto = CoreCrypto;
// for (let countCrytos = 0; countCrytos < 10; countCrytos += 1) {
//   const x = new CoreCrypto(true);
//   console.log(`contador for ${countCrytos}`);
// }
//# sourceMappingURL=core-crypto.js.map