"use strict";
/**
 * Filename: credential-type.ts
 * Author: rnunez@akurey.com
 * Date: 12/20/2018
 * Description: Enums to manage encryption and credentials
 */
Object.defineProperty(exports, "__esModule", { value: true });
var CredentialType;
(function (CredentialType) {
    CredentialType[CredentialType["CUSTOM"] = 1] = "CUSTOM";
    CredentialType[CredentialType["AWS"] = 2] = "AWS";
    CredentialType[CredentialType["AWS_COGNITO_AUTHO_TOKEN"] = 3] = "AWS_COGNITO_AUTHO_TOKEN";
    CredentialType[CredentialType["AWS_COGNITO_ID_TOKEN"] = 4] = "AWS_COGNITO_ID_TOKEN";
    CredentialType[CredentialType["POWER_BI"] = 5] = "POWER_BI";
})(CredentialType = exports.CredentialType || (exports.CredentialType = {}));
var KeyType;
(function (KeyType) {
    KeyType[KeyType["PRIVATE"] = 1] = "PRIVATE";
    KeyType[KeyType["PUBLIC"] = 2] = "PUBLIC";
})(KeyType = exports.KeyType || (exports.KeyType = {}));
//# sourceMappingURL=credential-type.js.map