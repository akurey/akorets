"use strict";
/**
 * Filename: secure-identity.ts
 * Author: rnunez@akurey.com
 * Date: 01/16/2019
 * Description: Class to store the user credential and it's cryptographic resources
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Object to securely be store, it keeps the association among the credential and it's private keys
 */
var SecureIdentity = /** @class */ (function () {
    function SecureIdentity(pCredential, pCrypto) {
        this.credential = pCredential;
        this.crypto = pCrypto;
    }
    SecureIdentity.prototype.getCredential = function () {
        return this.credential;
    };
    SecureIdentity.prototype.getCrypto = function () {
        return this.crypto;
    };
    return SecureIdentity;
}());
exports.SecureIdentity = SecureIdentity;
//# sourceMappingURL=secure-identity.js.map