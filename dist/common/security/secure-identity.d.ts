/**
 * Filename: secure-identity.ts
 * Author: rnunez@akurey.com
 * Date: 01/16/2019
 * Description: Class to store the user credential and it's cryptographic resources
 */
import { Credential } from "./credential";
import { CoreCrypto } from "./core-crypto";
/**
 * Object to securely be store, it keeps the association among the credential and it's private keys
 */
export declare class SecureIdentity {
    private credential;
    private crypto;
    constructor(pCredential: Credential, pCrypto: CoreCrypto);
    getCredential(): Credential;
    getCrypto(): CoreCrypto;
}
