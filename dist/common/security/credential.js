"use strict";
/**
 * Filename: credential.ts
 * Author: rnunez@akurey.com
 * Date: 12/20/2018
 * Description: Object to store user information regarding its identity, tokens and cryptograpy keys
 */
Object.defineProperty(exports, "__esModule", { value: true });
var Credential = /** @class */ (function () {
    function Credential(pUser, pPublicKey) {
        this.tokens = {};
        this.user = pUser;
        this.publicKey = pPublicKey;
        this.computer = '';
        this.ip = '';
        this.groupList = [];
    }
    Credential.prototype.addToken = function (pCredentialType, pValue) {
        this.tokens[pCredentialType] = pValue;
    };
    Credential.prototype.getToken = function (pCredentialType) {
        return this.tokens[pCredentialType] || '';
    };
    Credential.prototype.setDeviceName = function (pName) {
        this.computer = pName;
    };
    Credential.prototype.setDeviceIp = function (pIp) {
        this.ip = pIp;
    };
    return Credential;
}());
exports.Credential = Credential;
//# sourceMappingURL=credential.js.map