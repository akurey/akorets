/**
 * Filename: core-crypto.ts
 * Author: rnunez@akurey.com
 * Date: 12/20/2018
 * Description: Encrypt and decrypt functions based on RSA of auto generated keys
 */
export declare class CoreCrypto {
    private publicKey;
    private privateKey;
    /**
     * Creates a new instance capable of encrypt/decrypt
     * @param pGenerateNewKeys , if true, new keys are generated, by the contrary the keys must be provide properly
     */
    constructor(pGenerateNewKeys: boolean);
    getPublicKey(): string;
    getPrivateKey(): string;
    /**
     * Asymetric encryption based on given key
     * @param pData text to be encrypted
     * @param pKey RSA private/public key to be use for encryption
     */
    static encrypt(pData: string, pKey: string): string;
    /**
     * Asymetric decryption based on given key
     * @param pData text to be decrypted
     * @param pKey RSA private key to be use for decryption
     */
    static decrypt(pData: string, pPrivateKey: string): string;
    /**
     * Generate one asymetric key pair
     */
    private createKeyPair;
}
